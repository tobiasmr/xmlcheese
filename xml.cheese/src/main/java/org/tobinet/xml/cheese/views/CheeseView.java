package org.tobinet.xml.cheese.views;

import java.math.BigInteger;
import java.time.LocalDate;

import generated.Cheese;
import generated.Date;
import generated.Manufactur;
import generated.Manufacturer;
import generated.Sort;
import generated.Sorts;
import generated.Stars;
import generated.Taxonomies;
import generated.Taxonomy;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public final class CheeseView extends VBox {

    private final TextField cheeseName = new TextField();
    private final ChoiceBox<Taxonomies> taxonomyBox = new ChoiceBox<>();
    private final ChoiceBox<Sorts> sortBox = new ChoiceBox<>();
    private final ChoiceBox<Integer> starBox = new ChoiceBox<>();
    private final TextArea opinionArea = new TextArea();
    private final TextField manufacturerFirstname = new TextField();
    private final TextField manufacturerLastname = new TextField();
    private final DatePicker manufacturDate = new DatePicker();

    public CheeseView() {
        super();
        this.cheeseName.setPromptText("Cheese Name");
        this.taxonomyBox.getItems().addAll(Taxonomies.values());
        this.sortBox.getItems().addAll(Sorts.values());
        this.starBox.getItems().addAll(0, 1, 2, 3, 4, 5);
        this.opinionArea.setPromptText("Pers. Meinung");
        this.manufacturerFirstname.setPromptText("Hersteller Vorname");
        this.manufacturerLastname.setPromptText("Hersteller Nachname");

        this.getChildren().addAll(this.cheeseName, this.taxonomyBox, this.sortBox, this.starBox, this.opinionArea,
                this.manufacturerFirstname, this.manufacturerLastname, this.manufacturDate);
    }

    public Cheese save() {
        final Cheese cheese = new Cheese();

        final Stars star = new Stars();
        star.setValue(BigInteger.valueOf(this.starBox.getValue()));
        cheese.setBewertung(star);
        cheese.setName(this.cheeseName.getText());
        cheese.setMeinung(this.opinionArea.getText());
        final Taxonomy taxonomy = new Taxonomy();
        taxonomy.setValue(this.taxonomyBox.getValue());
        cheese.setTaxonomie(taxonomy);
        final Sort sort = new Sort();
        sort.setValue(this.sortBox.getValue());
        cheese.setSorte(sort);
        final Manufacturer hersteller = new Manufacturer();
        hersteller.setVorname(this.manufacturerFirstname.getText());
        hersteller.setNachname(this.manufacturerLastname.getText());
        cheese.setManufacturer(hersteller);
        final Manufactur herstellung = new Manufactur();
        cheese.setManufactur(herstellung);
        final Date date = new Date();
        herstellung.setManufacturDate(date);
        final LocalDate localDate = this.manufacturDate.getValue();
        date.setDay(BigInteger.valueOf(localDate.getDayOfMonth()));
        date.setMonth(BigInteger.valueOf(localDate.getMonthValue()));
        date.setYear(BigInteger.valueOf(localDate.getYear()));

        return cheese;
    }
}
