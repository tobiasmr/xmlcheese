package org.tobinet.xml.cheese;

import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tobinet.xml.cheese.utils.PathManager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Main class.
 *
 * @author tobias
 */
public final class Main extends Application {

    /** Logger. **/
    private static final Logger LOG = LogManager.getLogger();
    /** AppName. */
    private static final String APPNAME = "XML Cheese"; //$NON-NLS-1$
    /** Arguments. */
    private static final String ROOTPATH = "ROOTPATH"; //$NON-NLS-1$
    /** Arguments. */
    private static final Map<String, String> ARGUMENTS = new ConcurrentHashMap<>();

    /**
     * Main method
     *
     * @param args
     *            Arguments.
     */
    public static void main(final String[] args) {
        if (args != null) {

            Main.extractParam(args, Main.ROOTPATH);

            final String path = StringUtils.trimToEmpty(Main.ARGUMENTS.get(Main.ROOTPATH));
            PathManager.INSTANCE.setRootPath(path);
        }
        launch(args);
    }

    /*
     * (non-Javadoc)
     *
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        final String fxmlPath = "/fxml/Main.fxml"; //$NON-NLS-1$
        final FXMLLoader loader = new FXMLLoader();
        loader.setController(MainController.INSTANCE);
        Main.LOG.info("Loading FXML: " + fxmlPath); //$NON-NLS-1$
        final InputStream input = this.getClass().getResourceAsStream(fxmlPath);
        final BorderPane rootPane = (BorderPane) loader.load(input);
        final Scene scene = new Scene(rootPane);
        IOUtils.closeQuietly(input);

        primaryStage.initStyle(StageStyle.DECORATED);
        primaryStage.setTitle(Main.APPNAME);
        primaryStage.setScene(scene);
        primaryStage.setFullScreen(false);
        primaryStage.show();
    }

    /*
     * (non-Javadoc)
     *
     * @see javafx.application.Application#stop()
     */
    @Override
    public final void stop() throws Exception {
        MainController.INSTANCE.exit();
        super.stop();
    }

    /**
     * Method to extract parameters from CLI.
     *
     * @param args
     *            CLI arguments.
     * @param paramName
     *            Name of the parameter to extract.
     */
    protected static void extractParam(final String[] args, final String paramName) {
        final String prefix = "--" + paramName + '='; //$NON-NLS-1$
        for (final String arg : args) {
            final String param = StringUtils.trimToEmpty(arg);
            if (param.startsWith(prefix)) {
                Main.ARGUMENTS.put(paramName, param.substring(prefix.length()));
            }
        }
    }
}
