package org.tobinet.xml.cheese.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.EventFilter;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.dom.DOMResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public final class SerializerDeserializer {

    /** XMLFactory. */
    private static final XMLInputFactory INPUT_FACTORY = XMLInputFactory.newInstance();
    /** Whitespace filter . */
    private static final WhitespaceFilter WHITE_SPACE_FILTER = new WhitespaceFilter();

    /** map for caching the already loaded JAXBContext. */
    private static final Map<Class<?>, JAXBContext> JAXB_CTX_CACHE_MAP = new HashMap<>(); // NOPMD - not concurrent

    /**
     * Private constructor of the utility class.
     */
    private SerializerDeserializer() {
        super();
        SerializerDeserializer.INPUT_FACTORY.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false);
    }

    /**
     * Parse the given XML-file and put it into the given root java class.
     *
     * @param <T>
     *            the root element of the object tree
     * @param clazz
     *            the class of the root element
     * @param inputStream
     *            the path to the XML file
     * @param schemaUrl
     *            Schema location for schema check.
     * @return root element of the object tree
     * @throws IOException
     *             In case of a de-serialization error.
     */
    public static <T> T getPojoFromXML(final Class<T> clazz, final InputStream inputStream, final URL schemaUrl)
            throws IOException {
        try {
            // create the unmarshaller
            final Unmarshaller unmarshaller = SerializerDeserializer.createJaxbUnmarshaller(clazz, schemaUrl);

            // filter all whitespaces, this is done because of the good old forbidden trailingsequences at the end of a file, after
            // encryption and decryption
            final XMLEventReader eventReader1 = SerializerDeserializer.INPUT_FACTORY.createXMLEventReader(inputStream);
            final XMLEventReader eventReader2 = SerializerDeserializer.INPUT_FACTORY.createFilteredReader(eventReader1,
                    SerializerDeserializer.WHITE_SPACE_FILTER);

            return unmarshaller.unmarshal(eventReader2, clazz).getValue();
        } catch (final Exception exc) {
            throw new IOException(exc);
        }
    }

    /**
     * Getter.
     *
     * @param clazz
     *            XML class structure.
     * @return JAXB context object.
     * @throws JAXBException
     *             XML error.
     */
    public static <T> JAXBContext getJaxbContext(final Class<T> clazz) throws JAXBException {
        JAXBContext jContext = SerializerDeserializer.JAXB_CTX_CACHE_MAP.get(clazz);
        if (jContext == null) {
            jContext = JAXBContext.newInstance(clazz);
            SerializerDeserializer.JAXB_CTX_CACHE_MAP.put(clazz, jContext);
        }
        return jContext;
    }

    /**
     * Create method for the JAX-B unmarshaller.
     *
     * @param <T>
     *            the root element of the object tree
     * @param clazz
     *            the class of the root element
     * @param schemaUrl
     *            Schema location for schema check.
     * @return JAX-B Unmarshaller.
     * @throws JAXBException
     *             JAX-B error.
     * @throws SAXException
     *             XML error.
     */
    private static <T> Unmarshaller createJaxbUnmarshaller(final Class<T> clazz, final URL schemaUrl)
            throws JAXBException, SAXException {
        // get the JAX-B context object
        final JAXBContext jContext = SerializerDeserializer.getJaxbContext(clazz);
        // create the unmarshaller
        final Unmarshaller unmarshaller = jContext.createUnmarshaller();
        if (schemaUrl != null) {
            final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            final Schema schema = schemaFactory.newSchema(schemaUrl);
            unmarshaller.setSchema(schema);
        }
        return unmarshaller;
    }

    /**
     * Parse the given XML-file and put it into the given root java class.
     *
     * @param <T>
     *            the root element of the object tree
     * @param clazz
     *            the class of the root element
     * @param document
     *            DOM document to parse
     * @param schemaUrl
     *            Schema location for schema check.
     * @return root element of the object tree
     * @throws IOException
     *             In case of a de-serialization error.
     */
    public static <T> T getPojoFromDOM(final Class<T> clazz, final Document document, final URL schemaUrl)
            throws IOException {
        try {
            // create the unmarshaller
            final Unmarshaller unmarshaller = SerializerDeserializer.createJaxbUnmarshaller(clazz, schemaUrl);

            return unmarshaller.unmarshal(document, clazz).getValue();
        } catch (final Exception exc) {
            throw new IOException(exc);
        }
    }

    /**
     * Serializes a JAXB annotated POJO into an XML-OuputStream.
     *
     * @param clazz
     *            the class of the root element 4 the JAXB context.
     * @param pojo
     *            The POJO that should be serialized into the XML-Stream.
     * @param outputStream
     *            The stream that contains the resulting XML. (The return value!!!)
     * @param prettyPrint
     *            TRUE to enable the pretty print.
     * @throws IOException
     *             In case of a serialization error
     */
    public static void getXMLFromPojo(final Class<?> clazz, final Object pojo, final OutputStream outputStream,
            final boolean prettyPrint) throws IOException {
        try {
            // get the JAX-B context object
            final JAXBContext jContext = SerializerDeserializer.getJaxbContext(clazz);
            // create the marshaller
            final Marshaller marshaller = jContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.valueOf(prettyPrint));
            marshaller.marshal(pojo, outputStream);
        } catch (final JAXBException exc) {
            throw new IOException(exc);
        }
    }

    /**
     * Getter.
     *
     * @param clazz
     *            the class of the root element 4 the JAXB context.
     * @param pojo
     *            The POJO that should be serialized into the XML-Stream.
     * @return The XML document based on the POJO.
     * @throws IOException
     *             In case of a serialization error
     */
    public static Document getDomFromPojo(final Class<?> clazz, final Object pojo) throws IOException {
        try {
            // get the JAX-B context object
            final JAXBContext jContext = SerializerDeserializer.getJaxbContext(clazz);
            // create the marshaller
            final Marshaller marshaller = jContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

            final DOMResult res = new DOMResult();
            marshaller.marshal(pojo, res);
            return (Document) res.getNode();
        } catch (final JAXBException exc) {
            throw new IOException(exc);
        }
    }

    /**
     * This is a filter for whitespace filtering while reading the xml.
     */
    private static final class WhitespaceFilter implements EventFilter {

        /*
         * (non-Javadoc)
         *
         * @see javax.xml.stream.EventFilter#accept(javax.xml.stream.events.XMLEvent)
         */
        @Override
        public boolean accept(final XMLEvent event) {
            return !(event.isCharacters() && ((Characters) event).isWhiteSpace());
        }
    }
}
