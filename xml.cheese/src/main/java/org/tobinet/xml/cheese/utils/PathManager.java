package org.tobinet.xml.cheese.utils;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lombok.Getter;

/**
 * @author tobias
 */
public final class PathManager {

    /** Logger. */
    private static final Logger LOG = LogManager.getLogger();

    /** Singleton. */
    public static final PathManager INSTANCE = new PathManager();

    /** Path to root Directory. */
    @Getter
    private String rootPath = "./"; //$NON-NLS-1$

    /**
     * Private constructor for singleton.
     *
     */
    private PathManager() {
        super();
    }

    /**
     * Setter.
     *
     * @param path
     *            path from Arguments.
     */
    public void setRootPath(final String path) {
        if (StringUtils.isNotEmpty(path)) {
            this.rootPath = path + (StringUtils.endsWith(path, File.separator) ? StringUtils.EMPTY : File.separator);
        }
        PathManager.LOG.info("Home Directory: " + this.rootPath); //$NON-NLS-1$
    }
}
