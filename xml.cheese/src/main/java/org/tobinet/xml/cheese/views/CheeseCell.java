package org.tobinet.xml.cheese.views;

import generated.Cheese;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

public final class CheeseCell extends ListCell<Cheese> {

    public CheeseCell() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
     */
    @Override
    protected void updateItem(final Cheese bean, final boolean empty) {
        if (empty) {
            this.setGraphic(null);
        } else {
            this.setGraphic(new Label(bean.getName()));
        }
    }
}
