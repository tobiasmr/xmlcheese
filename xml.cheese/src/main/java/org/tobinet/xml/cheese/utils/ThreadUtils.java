package org.tobinet.xml.cheese.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author tobias
 *
 */
public final class ThreadUtils {

    /** Logger. **/
    private static final Logger LOG = LogManager.getLogger();

    /** Singleton. */
    public static final ThreadUtils INSTANCE = new ThreadUtils();

    /** Wait Time. */
    private static final int WAITTIME = 100;
    /** Thread Group. */
    private final ThreadGroup threadGroup = new ThreadGroup("tnet"); //$NON-NLS-1$

    /**
     * Default Constructor.
     */
    private ThreadUtils() {
        super();
    }

    /**
     * Create new Thread in ThreadGroup.
     *
     * @param target
     *            Runnable.
     * @return new Thread.
     */
    public Thread createThread(final Runnable target) {
        return new Thread(this.threadGroup, target);
    }

    /**
     * Interrupt all Threads in Group.
     */
    public void interruptThreads() {
        synchronized (this.threadGroup) {
            this.threadGroup.interrupt();
        }
    }

    /**
     * Join all Threads.
     */
    public void joinAll() {
        synchronized (this.threadGroup) {
            while (this.threadGroup.activeCount() > 0) {
                try {
                    this.threadGroup.wait(ThreadUtils.WAITTIME);
                } catch (final Exception ex) {
                    ThreadUtils.LOG.error(ex, ex);
                }
            }
        }
    }
}
