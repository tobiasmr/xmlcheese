package org.tobinet.xml.cheese;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tobinet.xml.cheese.utils.ImportExporter;
import org.tobinet.xml.cheese.utils.ThreadUtils;
import org.tobinet.xml.cheese.views.CheeseCell;
import org.tobinet.xml.cheese.views.CheeseView;

import generated.Cheese;
import generated.CheeseList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

/**
 * @author tobias
 */
public final class MainController implements Initializable {

    /** Logger. */
    private static final Logger LOG = LogManager.getLogger();

    /** Singleton. */
    public static final MainController INSTANCE = new MainController();

    /**
     * main BorderPane.
     */
    @FXML
    private BorderPane mainPane;

    final ListView<Cheese> cheeseList = new ListView<>();

    /**
     * private Constructor.
     */
    private MainController() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        MainController.LOG.info("Initializing MainController"); //$NON-NLS-1$

        final CheeseView cheeseView = new CheeseView();
        this.mainPane.setCenter(cheeseView);
        final Button saveButton = new Button();
        saveButton.setText("Save");
        saveButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(final ActionEvent event) {
                final Cheese newCheese = cheeseView.save();
                MainController.this.cheeseList.getItems().add(newCheese);
                final CheeseList list = new CheeseList();
                list.getCheese().addAll(MainController.this.cheeseList.getItems());
                ImportExporter.INSTANCE.export(list);
            }
        });
        this.mainPane.setBottom(saveButton);

        this.cheeseList.setCellFactory(new Callback<ListView<Cheese>, ListCell<Cheese>>() {

            @Override
            public ListCell<Cheese> call(final ListView<Cheese> param) {
                return new CheeseCell();
            }
        });
        this.mainPane.setRight(this.cheeseList);

        this.cheeseList.getItems().addAll(ImportExporter.INSTANCE.importDefault().getCheese());
    }

    /**
     * Exit the application.
     */
    public void exit() {
        MainController.LOG.info("Exiting application..."); //$NON-NLS-1$

        ThreadUtils.INSTANCE.interruptThreads();
        MainController.LOG.info("Bye."); //$NON-NLS-1$
    }
}
