package org.tobinet.xml.cheese.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import generated.CheeseList;

public final class ImportExporter {

    /** Logger. */
    private static final Logger LOG = LogManager.getLogger();

    private static final String DEFAULTFILENAME = "out.xml";

    /** Singleton. */
    public static final ImportExporter INSTANCE = new ImportExporter();

    /**
     *
     */
    private ImportExporter() {
        super();
    }

    public void export(final CheeseList cheeseList) {
        OutputStream out = null;
        try {
            cheeseList.setVersion(cheeseList.getVersion());
            final File outFile = new File(PathManager.INSTANCE.getRootPath() + DEFAULTFILENAME);
            out = new FileOutputStream(outFile);
            SerializerDeserializer.getXMLFromPojo(CheeseList.class, cheeseList, out, true);
            out.flush();
            LOG.info("Saved to: " + outFile.getAbsolutePath());
        } catch (final IOException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    public CheeseList importDefault() {
        return this.importFile(new File(PathManager.INSTANCE.getRootPath() + DEFAULTFILENAME));
    }

    public CheeseList importFile(final File file) {
        InputStream input = null;
        CheeseList cheeselist = new CheeseList();
        try {
            input = new FileInputStream(file);
            final URL schemaUrl = this.getClass().getResource("/cheese.xsd");
            cheeselist = SerializerDeserializer.getPojoFromXML(CheeseList.class, input, schemaUrl);

        } catch (final IOException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(input);
        }
        return cheeselist;
    }
}
